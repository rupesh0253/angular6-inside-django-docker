from django.conf.urls import url
from registration.views import login

from . import views
# from .views import Owner

urlpatterns = [
    url(r'^$', views.HomePageView.as_view()),
    url(r'^links/$', views.LinksPageView.as_view()),
    url(r'^getcust/$',views.Customers.getCust),
    url(r'^hit/$', login),
]